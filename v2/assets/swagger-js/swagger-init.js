if ('undefined' !== typeof module) {

    module.exports = function initswagger(){
        let url = 'https://apiv2.favoriot.com/api-docs';
        initSwaggerUi(url);
    }
    
    function initSwaggerUi(url) {
        // Pre load translate...
        if (window.SwaggerTranslator) {
            window.SwaggerTranslator.translate();
        }
        window.swaggerUi = new SwaggerUi({
            url: url,
            dom_id: "swagger-ui-container",
            supportedSubmitMethods: ['get', 'post', 'put', 'delete', 'patch'],
            onComplete: function (swaggerApi, swaggerUi) {
                if (typeof initOAuth == "function") {
                    initOAuth({
                        clientId: "your-client-id",
                        clientSecret: "your-client-secret-if-required",
                        realm: "your-realms",
                        appName: "your-app-name",
                        scopeSeparator: ",",
                        additionalQueryStringParams: {}
                    });
                }

                if (window.SwaggerTranslator) {
                    window.SwaggerTranslator.translate();
                }

                addApiKeyAuthorization();
            },
            onFailure: function (data) {
                alert("Unable to Load SwaggerUI");
            },
            docExpansion: "none",
            jsonEditor: false,
            // apisSorter: "alpha",
            // operationsSorter : "alpha",          
            defaultModelRendering: 'schema',
            showRequestHeaders: false
        });

        function addApiKeyAuthorization() {
            var key = encodeURIComponent($('#input_apiKey')[0].value);
            if (key && key.trim() != "") {
                var apiKeyAuth = new SwaggerClient.ApiKeyAuthorization("apikey", key, "header");
                window.swaggerUi.api.clientAuthorizations.add("apiKey", apiKeyAuth);

                // var apiKeyAuth = new SwaggerClient.ApiKeyAuthorization("api_key", key, "query");
                // window.swaggerUi.api.clientAuthorizations.add("api_key", apiKeyAuth);

                // alert("added key " + key);
            }
        }

        $('#input_apiKey').change(addApiKeyAuthorization);

        // if you have an apiKey you would like to pre-populate on the page for demonstration purposes...
        /*
           var apiKey = "myApiKeyXXXX123456789";
           $('#input_apiKey').val(apiKey);
         */

        window.swaggerUi.load();

        // function log() {
        //     if ('console' in window) {
        //         console.log.apply(console, arguments);
        //     }
        // }
    }


   

}